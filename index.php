<?php 
/*
MIT License
Copyright (c) 2023 Deevad

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

echo '<!DOCTYPE html>'."\n";
echo '<html>'."\n";
echo '<head>'."\n";
echo '<title>Demo of comments synchronised from Mastodon in Php</title>'."\n";
echo '</head>'."\n";
echo '<body>'."\n";


# JSON Mastodon comment reader
# ----------------------------
echo '<div style="max-width:900px; margin: 2rem auto;">'."\n";

echo '<h1>Demo of comments synchronized from Mastodon in Php</h1>'."\n";

echo 'More info about the method on <a href="https://www.davidrevoy.com/article981/">this blog-post</a>.<br/>'."\n";
echo 'Demo on <a href="https://www.peppercarrot.com/extras/html/2023_Mastodon-Php-Comments/">this page</a>.<br/>'."\n";
echo 'source code on <a href="https://framagit.org/Deevad/mastocomblog">this repository</a>.<br/>'."\n";

echo '<h2>The post:</h2>'."\n";

echo '<a href="https://www.peppercarrot.com/en/viewer/misc__2023-17-16_My-Neighboor-Mastodon_25K_by-David-Revoy.html">'."\n";
echo '<img src="https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2023-17-16_My-Neighboor-Mastodon_25K_by-David-Revoy.jpg" style="width:100%;margin: 5px 30px 5px 10px;">'."\n";
echo '</a>'."\n";
echo '</p>'."\n";
echo '🌱 My Neighbor Mastodon 3<br/><br/>'."\n";
echo 'Thanks, thanks, *thanks* for being more than 25K to follow my art here!<br/><br/>'."\n";
echo '<p>'."\n";

echo '<div style="text-align:left;">'."\n";

echo '<h2>The comments</h2>'."\n";

# SETUP
# -----
# The adress of the post, manually copied from your post on Mastodon (18 digit, no space)
$mastodon_post_id = "110724917901550178";
# URL of your Mastodon instance (not ending with '/' and without 'https://')
$mastodon_server = 'framapiaf.org';
# Your mastodon login name:
$mastodon_account = 'davidrevoy';
# Private token generated from Mastodon Settings -> Development.
# Empty = public access, limited to the 60 first comments.
$your_mastodon_access_token = '';
# Limitation of character lenght for instance allowing long message, and so: a possibility of flooding your blog.
$maximum_character_length_for_content = 1600;

# Universal error/missing comment message
function _comment_error() {
  echo '<center>'."\n";
  echo '<div style="color:#999;max-width:600px;margin-top: 20px;">'."\n";
  echo '<br/>'."\n";
  echo '<h3>Error</h3>'."\n";
  echo 'Something went wrong.'."\n";
  echo '</div>'."\n";
  echo '</center>'."\n";
}

# A function used in case of trimming arbitrary a message, it can break HTML in $content 
# and leave unclosed tag (disastrous for your comments if a <a href is left open, or a span...Etc...) 
# src: https://stackoverflow.com/questions/3810230/close-open-html-tags-in-a-string , by Markus and with slight changes by Deevad.
function trim_and_close_tags($html, $length){
    $trimming = substr($html, $length);
    $pos = strpos($trimming, ">");
    if($pos !== false) {
        $html = substr($html, 0,$length + $pos + 1);
    } else {
        $html = substr($html, 0,$length);
    }
    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i=0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</'.$openedtags[$i].'>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

# Filter the string to avoid issues
$mastodon_post_id = preg_replace('/[^0-9]/', '', $mastodon_post_id);
if ( strlen($mastodon_post_id) == 18 ) {
  if (preg_match('/\S/', $mastodon_post_id)) {

    # Endpoint of the API
    $API_endpoint = '/api/v1/statuses/'.$mastodon_post_id.'/context';
    # Final API url
    $mastodon_API_url = 'https://'.$mastodon_server.''.$API_endpoint.'';
    # Full web adress of the post
    $mastodon_final_post_url = 'https://'.$mastodon_server.'/@'.$mastodon_account.'/'.$mastodon_post_id;

    # CACHE
    # -----
    # Download API only once every 24h and save it on disk
    # 1 call per day is friendly for your Mastodon server <3
    $cache_target = __DIR__.'/'.$mastodon_post_id.'-data.json';
    # The setting, in minutes (60min x 24h = 1440)
    $cache_limit_in_mins = 1440; 

    # Get a version in hours, to print on the footer as a hint of refresh for user.
    $cache_limit_human_readable = round($cache_limit_in_mins / 60);

    # UPDATE API data
    # ---------------
    # Function that download the remote JSON and store it to cache
    function _update_cache() {
      global $cache_target;
      global $your_mastodon_access_token;
      global $mastodon_API_url;

      # If we have a token, let's use an authentified method to not be limited by public 60 comments maximum.
      if (!empty($your_mastodon_access_token)) {

        # Header for the call
        $headers = [
          'Authorization: Bearer '.$your_mastodon_access_token,
          'Content-Type: multipart/form-data',
        ];

        # Call using Curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $mastodon_API_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $reply = curl_exec($ch);

        if (!$reply) {
          return json_encode(['ok'=>false, 'curl_error_code' => curl_errno($ch), 'curl_error' => curl_error($ch)]);
        }
        curl_close($ch);

        $remote_json = $reply;

      } else {
        # Public call to the API, limited to 60 comments maximum.
        $remote_json = file_get_contents($mastodon_API_url.'?'.mt_rand());
      }

      $file = fopen ( $cache_target, 'w');
      fwrite ( $file, $remote_json );
      fclose ( $file );

      chmod($cache_target, 0620);
    }

    # CACHE File Management
    # ---------------------
    if (!file_exists($cache_target)) {
      # No file found in cache, so we download a new one:
      _update_cache();
    } else {
      # We have a cache; calculate if the files is outdated
      $diff_in_secs = (time() - (60 * $cache_limit_in_mins)) - filemtime($cache_target);
      if ( $diff_in_secs > 0 ) {
        # Outdated file, so we update it
        _update_cache();
      }
    }

    # Decode our local json in cache
    $local_json = file_get_contents($cache_target);
    $data = json_decode($local_json);

    # Display Mastodon comments (loop)
    foreach ($data->descendants as $id) {
       # Do not allow displaying private (followers only) and direct (DM) post, only accept public and unlisted.
      $accepted_visibility_status = array ('public', 'unlisted');
      if(in_array($id->visibility, $accepted_visibility_status, true)) {
        # Reset variable each loop
        $comment_level = 0;

        # PARSE INFOS FROM JSON
        ##---------------------
        $author = $id->account->username;
        $author = htmlspecialchars($author);
        $display_name = $id->account->display_name;
        $display_name = htmlspecialchars($display_name);
        # Color decoration on Display name
        # for the :custom_emojis:
        $display_name = preg_replace('/ :[^:\s]*(?:::[^:\s]*)*:/','<i style="color:#b1c0e7; font-size:0.9rem">$0</i>', $display_name);
        # for suffix/status between parenthesis, eg (he/him), (personal account), (new account) etc... :
        $display_name = preg_replace('/\(([^\)]+)\)/','<i style="color:#b1c0e7; font-size:0.9rem">$0</i>', $display_name);
        $account =  $id->account->acct;
        $account =  htmlspecialchars($account);
        $author_url = $id->account->url;
        $avatar = $id->account->avatar;
        $favourites_count = $id->favourites_count;
        $comment_url_on_masto = $id->url;
        $comment_id_on_masto = $id->id;
        $comment_creation_time = $id->created_at;
        $comment_creation_time = str_replace('T', ' ', $comment_creation_time);
        $comment_creation_time = substr($comment_creation_time,0,16).'';
        # check if it is a reply to previous ID.
        $comment_in_reply_to_id = $id->in_reply_to_id;
        if ($comment_in_reply_to_id != $mastodon_post_id ) {
          $comment_level = 100;
        }
        $content = $id->content;
        $content_allowed_html = '<b><a><span><p><em><strong><b><i><u><br><hr><ul><ol><li><img>';
        $content = strip_tags($content, $content_allowed_html);

        # Cleanup systematic quote of your account at start of every message
        $OP_html_string = '@<span>'.$mastodon_account.'</span>';
        $content = str_replace($OP_html_string, '', $content);
        # Decorate :custom_emojis: on content
        $content = preg_replace('/ :[^:\s]*(?:::[^:\s]*)*:/','<i style="color:#5e9ed6;">$0</i>', $content);
        # Prevent flood
        if (strlen($content)>=$maximum_character_length_for_content) {
          # Content is longer: trim it, and display an invitation to read it on the real instance.
          $content = trim_and_close_tags($content, $maximum_character_length_for_content).'<small><em style="color:#81749c;">(Original message has been truncated: <a href="'.$comment_url_on_masto.'" target="_blank" style="color:#81749c; text-decoration: underline;"> read the complete original message here</a>.)</em></small>';
        }

        # DISPLAY THE COMMENTS
        ##--------------------
        #A comment box
        echo '<div style="border: 1px solid #ccc;padding: 10px; border-radius: 20px; margin:10px 10px 10px '.$comment_level.'px; min-height: 100px;">'."\n";

          echo '<img src="'.$avatar.'" style="width:100px;margin: 5px 30px 5px 10px; float:left;">';

          # Author's name
          echo '  <strong>'."\n";
          echo '    <a href="'.$author_url.'" style="color:#222;">'."\n";
          echo '      <b>'.$display_name.'</b>'."\n";
          echo '    </a>'."\n";
          echo '  </strong>'."\n";

          echo '  <small>'."\n";

          # Write user mastodon @ adress in small under their choosen nickmane; because they quote each other with adress all the time.
          echo '  <br/>';
          echo '  <span style="font-size:0.9rem; color: #958db9;">'.$account.'</span>'."\n";
          echo '  - '."\n";

          # Date
          echo '    <time datetime="'.$comment_creation_time.'">'."\n";
          echo '      '.$comment_creation_time.''."\n";
          echo '    </time>'."\n";
          echo '    -'."\n";
          # Reply button
          echo '    <a rel="nofollow" href="'.$comment_url_on_masto.'">'."\n";
          echo '      Reply'."\n";
          echo '    </a>'."\n";
          echo '  </small>'."\n";

          # Content
          echo '  <p>'."\n";
          echo '    '.$content.''."\n";

          # Attachments
          $attachments = array();
          foreach ($id->media_attachments as $media) {
            $file = basename($media->url);
            # Pictures / Arts / Photos
            if ( $media->type == "image" ) {
              echo '🖼️ <a href="'.$media->url.'" title="'.$media->description.'" alt="'.$media->description.'" target="_blank">'.$file.'</a>&nbsp;'."\n";
            # Videos
            } elseif ( $media->type == "video" ) {
              echo '📽️ <a href="'.$media->url.'" title="'.$media->description.'" alt="'.$media->description.'" target="_blank">'.$file.'</a>&nbsp;'."\n";
            # Everything else
            } else {
              echo '📎 <a href="'.$media->url.'" title="'.$media->description.'" alt="'.$media->description.'" target="_blank">'.$file.'</a>&nbsp;'."\n";
            }
          }

          # fav
          if ( $favourites_count > 0 ) {
              echo '<span style="color: #bbb; border: 1px solid #ddd; text-align:center; padding: 2px 6px 3px 6px; border-radius: 10px;">';
              echo ''.$favourites_count.' ★';
              echo '</span>'."\n";
          }

          echo '  </p>'."\n";
          echo '</div>'."\n";

      } // end of checking status of comment (not allowing private (followers only) and direct (DM))
    } // end for loop

    echo '<center>'."\n";
    echo '<div style="color:#999;max-width:640px;margin-top: 20px;">'."\n";
    echo 'The comments are synchronised every '.$cache_limit_human_readable.'h with the replies <a href="'.$mastodon_final_post_url.'" title="" target="_blank">to this post on Mastodon</a>:'."\n";
    echo '<br/>'."\n";
    echo '<pre>'.$mastodon_final_post_url.'</pre>'."\n";
    echo '</div>'."\n";
    echo '</center>'."\n";


  } else { // we don't have masto_id for this article

  _comment_error();

  } // end if masto_id


} else { // if mastoid is malformated

  _comment_error();

} // end mastoid malformated check
echo '</div>'."\n";
?>
