# MastoComBlog

**"My Mastodon comments on my Blog"**

A Php script that fetch and displays the comments received from the Fediverse. It requires a Mastodon account and posting on it to get the post ID.

Repository: https://framagit.org/Deevad/mastocomblog/

![](https://www.davidrevoy.com/data/images/blog/2023/2023-07-26_mastodon-mascot-as-postman.jpg)

## Demo page

[Demo of the Php code (public mode).](https://www.peppercarrot.com/extras/html/2023_Mastodon-Php-Comments/)

## Setup

The script needs to be in a directory with write permissions to write the cached json files. You can customize the path to this directory with `$cache_target` at the start of the CACHE section in the code.

`$mastodon_post_id`  The 18 numbers of your Mastodon post. You'll find this number on the URL of your post on Mastodon.  
`$mastodon_server` Your Mastodon instance URL.  
`$mastodon_account` Your account.  

**Token and authentification:**  
`$your_mastodon_access_token` (optional) This is a token you can get in your Mastodon account by going to Settings → Development and registering your site here. Without this token, the API will run in "public mode" and will limit the number of messages you can list (~60 for my instance). Instances of Mastodon might have no public mode enabled. In this case the authentication token method might be mandatory to connect to your messages.

**Cache:**  I wrote a caching system because with more than 13K daily visitors to my blog (which sometimes peaks at over 300K), I did not want to be a problem for the bandwidth of my Mastodon instance. The default in the code is set to 24h. You can extend it with something more complex if your blog system gives you a date. I use 1h for this week's blog post, then 4h for the month and 12h after that. Just be kind to the bandwidth of your Mastodon instance and donate to them to help cover the server costs.    

## Inspiration

The idea is detailed on [this blog-post](https://www.davidrevoy.com/article981/i-may-have-found-an-alternative-solution-to-my-blogs-comment-system) and a previous blog-post.

The inspiration for this system came from Carl Schwan's blog post ["Adding comments to your static blog with Mastodon"](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/). If you are looking for a Javascript client-side method, check out his post. 

I also found a real-life example of authentication by token to be very useful, found on ["MastodonBotPHP"](https://github.com/Eleirbag89/MastodonBotPHP/tree/master) by Grimstack aka Eleirbag89 on Github.

Also very useful is [the official Mastodon API documentation](https://docs.joinmastodon.org/entities/Token/), well done to the contributors who maintain this kind of detailed documentation.

## License

MIT License  
Copyright (c) 2023 Deevad  

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
